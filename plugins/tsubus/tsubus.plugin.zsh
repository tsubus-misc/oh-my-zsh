source /usr/share/doc/pkgfile/command-not-found.zsh

#virtualenv2 venv
#source ~/venv/bin/activate

export EDITOR=vim
export GIT_EDITOR=$EDITOR
export SVN_EDITOR=$EDITOR
export VISUAL=$EDITOR
export PATH="/usr/lib/ccache/bin/:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$PATH:$HOME/go/bin"
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"

#source /usr/share/nvm/init-nvm.sh
alias vi=vim
alias ssh='ssh -Y'
alias gfu='git fetch upstream'
alias ptg='push-to-gerrit.sh'
alias fptg='force-push-to-gerrit.sh'
alias rfptg='really-force-push-to-gerrit.sh'
